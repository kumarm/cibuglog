commit 87b65d5b8a0f85a0950230e1459b8cfd334ba5ed
Author: Martin Peres <martin.peres@linux.intel.com>
Date:   Thu Dec 28 19:29:02 2017 +0200

    js/bootstrap-duallistbox: add the setSelection method

    This will allow programatically setting a list of values, which helps for auto-filing.

    This is a port of 3cb26c96f4c51db4528dfc5a9ea6817890f3026f to duallistbox 4.0.1.

diff --git a/CIResults/static/js/jquery.bootstrap-duallistbox.js b/CIResults/static/js/jquery.bootstrap-duallistbox.js
index 3b47363..2f7118d 100644
--- a/CIResults/static/js/jquery.bootstrap-duallistbox.js
+++ b/CIResults/static/js/jquery.bootstrap-duallistbox.js
@@ -333,6 +333,35 @@
     triggerChangeEvent(dualListbox);
   }

+  function setSelection(dualListbox, values) {
+      if (dualListbox.settings.preserveSelectionOnMove === 'all' && !dualListbox.settings.moveOnSelect) {
+        saveSelections(dualListbox, 1);
+        saveSelections(dualListbox, 2);
+      } else if (dualListbox.settings.preserveSelectionOnMove === 'moved' && !dualListbox.settings.moveOnSelect) {
+        saveSelections(dualListbox, 2);
+      }
+
+      const vals = new Set(values)
+      dualListbox.element.find('option').each(function(index, item) {
+        var $item = $(item)
+        if (vals.has($item.attr('value'))) {
+          $item.prop('selected', true);
+          $item.attr('data-sortindex', dualListbox.sortIndex);
+          dualListbox.sortIndex++;
+        } else {
+          $item.prop('selected', false);
+          $item.removeAttr('data-sortindex');
+        }
+      });
+
+      refreshSelects(dualListbox);
+      triggerChangeEvent(dualListbox);
+      sortOptions(dualListbox.elements.select1);
+      if(dualListbox.settings.sortByInputOrder){
+          sortOptionsByInputOrder(dualListbox.elements.select2);
+      }
+  }
+
   function bindEvents(dualListbox) {
     dualListbox.elements.form.submit(function(e) {
       if (dualListbox.elements.filterInput1.is(':focus')) {
@@ -834,6 +863,9 @@
       this.element.show();
       $.data(this, 'plugin_' + pluginName, null);
       return this.element;
+    },
+    setSelection: function(values) {
+      setSelection(this, values);
     }
   };


commit 0f8350d25b9c29a14de5101fd00203e607791a31
Author: Martin Peres <martin.peres@linux.intel.com>
Date:   Thu Dec 13 16:11:19 2018 +0200

    duallistbox: fix the style until we move to bootstrap 4.x

diff --git a/CIResults/static/css/bootstrap-duallistbox.min.css b/CIResults/static/css/bootstrap-duallistbox.min.css
index 57d03f6..6a8865a 100644
--- a/CIResults/static/css/bootstrap-duallistbox.min.css
+++ b/CIResults/static/css/bootstrap-duallistbox.min.css
@@ -1 +1 @@
-.bootstrap-duallistbox-container .buttons{width:100%;margin-bottom:-1px}.bootstrap-duallistbox-container label{display:block}.bootstrap-duallistbox-container .info{display:inline-block;margin-bottom:5px;font-size:11px}.bootstrap-duallistbox-container .clear1,.bootstrap-duallistbox-container .clear2{display:none;font-size:10px}.bootstrap-duallistbox-container .box1.filtered .clear1,.bootstrap-duallistbox-container .box2.filtered .clear2{display:inline-block}.bootstrap-duallistbox-container .move,.bootstrap-duallistbox-container .remove{width:50%;box-sizing:content-box}.bootstrap-duallistbox-container .btn-group .btn{border-bottom-left-radius:0;border-bottom-right-radius:0}.bootstrap-duallistbox-container:not(.moveonselect) select{border-top-left-radius:0;border-top-right-radius:0}.bootstrap-duallistbox-container .moveall,.bootstrap-duallistbox-container .removeall{width:50%;box-sizing:content-box}.bootstrap-duallistbox-container.bs2compatible .btn-group>.btn+.btn{margin-left:0}.bootstrap-duallistbox-container select{width:100%;height:300px;padding:0}.bootstrap-duallistbox-container .filter{display:inline-block;width:100%;height:31px;margin:0 0 5px 0;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}.bootstrap-duallistbox-container .filter.placeholder{color:#aaa}.bootstrap-duallistbox-container.moveonselect .move,.bootstrap-duallistbox-container.moveonselect .remove{display:none}.bootstrap-duallistbox-container.moveonselect .moveall,.bootstrap-duallistbox-container.moveonselect .removeall{width:100%}
+.bootstrap-duallistbox-container .buttons{width:100%;margin-bottom:-1px}.bootstrap-duallistbox-container label{display:block}.bootstrap-duallistbox-container .info{display:inline-block;margin-bottom:5px;font-size:11px}.bootstrap-duallistbox-container .clear1,.bootstrap-duallistbox-container .clear2{display:none;font-size:10px}.bootstrap-duallistbox-container .box1.filtered .clear1,.bootstrap-duallistbox-container .box2.filtered .clear2{display:inline-block}.bootstrap-duallistbox-container .move,.bootstrap-duallistbox-container .remove{width:50%;}.bootstrap-duallistbox-container .btn-group .btn{border-bottom-left-radius:0;border-bottom-right-radius:0}.bootstrap-duallistbox-container:not(.moveonselect) select{border-top-left-radius:0;border-top-right-radius:0}.bootstrap-duallistbox-container .moveall,.bootstrap-duallistbox-container .removeall{width:50%;}.bootstrap-duallistbox-container.bs2compatible .btn-group>.btn+.btn{margin-left:0}.bootstrap-duallistbox-container select{width:100%;height:300px;padding:0}.bootstrap-duallistbox-container .filter{display:inline-block;width:100%;height:31px;margin:0 0 5px 0;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}.bootstrap-duallistbox-container .filter.placeholder{color:#aaa}.bootstrap-duallistbox-container.moveonselect .move,.bootstrap-duallistbox-container.moveonselect .remove{display:none}.bootstrap-duallistbox-container.moveonselect .moveall,.bootstrap-duallistbox-container.moveonselect .removeall{width:100%}


commit 42ab225b552b0f4e72936ad366772d357488a879
Author: Martin Peres <martin.peres@linux.intel.com>
Date:   Thu Apr 26 16:24:40 2018 +0300

    duallistbox/saveSelections: avoid using jquery when possible

    This provides a speedup of 6 in test case.

    This is a port of 9b13ca4fbf011fc5909e3d51adcd26e7632a4e48 to
    duallistbox 4.0.1.

diff --git a/CIResults/static/js/jquery.bootstrap-duallistbox.js b/CIResults/static/js/jquery.bootstrap-duallistbox.js
index 06aa765..3b47363 100644
--- a/CIResults/static/js/jquery.bootstrap-duallistbox.js
+++ b/CIResults/static/js/jquery.bootstrap-duallistbox.js
@@ -210,8 +210,7 @@
   function saveSelections(dualListbox, selectIndex) {
     var options = dualListbox.element.find('option');
     dualListbox.elements['select'+selectIndex].find('option').each(function(index, item) {
-      var $item = $(item);
-      options.eq($item.data('original-index')).data('_selected', $item.prop('selected'));
+      options.eq(item.getAttribute('data-original-index')).data('_selected', item.selected);
     });
   }


commit 1be15351c01e217d340a6a68e2d2c78c1f6b5c40
Author: Martin Peres <martin.peres@linux.intel.com>
Date:   Thu Dec 13 15:55:54 2018 +0200

    duallistbox/filter: do not create a list before going through each item

    This saves a ton of time and makes the list usable with tens of
    thousands of entries.

    This is a direct port of f123ba1dc21c9b2e08de0243020e633e74ed2166 to
    bootstrap-duallistbox 4.0.1.

diff --git a/CIResults/static/js/jquery.bootstrap-duallistbox.js b/CIResults/static/js/jquery.bootstrap-duallistbox.js
index 0975f96..06aa765 100644
--- a/CIResults/static/js/jquery.bootstrap-duallistbox.js
+++ b/CIResults/static/js/jquery.bootstrap-duallistbox.js
@@ -180,14 +180,7 @@

     dualListbox.elements['select'+selectIndex].empty().scrollTop(0);
     var regex,
-      allOptions = dualListbox.element.find('option'),
-      options = dualListbox.element;
-
-    if (selectIndex === 1) {
-      options = allOptions.not(':selected');
-    } else  {
-      options = options.find('option:selected');
-    }
+      allOptions = dualListbox.element.find('option');

     try {
       regex = new RegExp($.trim(dualListbox.elements['filterInput'+selectIndex].val()), 'gi');
@@ -197,7 +190,11 @@
       regex = new RegExp('/a^/', 'gi');
     }

-    options.each(function(index, item) {
+    allOptions.each(function(index, item) {
+      if ((selectIndex === 1 && item.selected) || (selectIndex === 2 && !item.selected)) {
+        return true;
+      }
+
       var $item = $(item),
         isFiltered = true;
       if (item.text.match(regex) || (dualListbox.settings.filterOnValues && $item.attr('value').match(regex) ) ) {
