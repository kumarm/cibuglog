from django import forms
from django.db import transaction
from django.utils.functional import cached_property

from collections import OrderedDict

from .models import Test


class TestMassRenameForm(forms.Form):
    substring_from = forms.CharField()
    substring_to = forms.CharField()

    @cached_property
    def affected_tests(self):
        tests = OrderedDict()

        if self.is_valid():
            substring_from = self.cleaned_data.get('substring_from')
            substring_to = self.cleaned_data.get('substring_to')

            if substring_from is not None and substring_to is not None:
                tmp = Test.objects.order_by('testsuite__name', 'name').prefetch_related('testsuite')
                for test in tmp.filter(name__contains=substring_from):
                    new_name = test.name.replace(substring_from, substring_to)
                    tests[test] = new_name

        return tests

    @transaction.atomic
    def do_renaming(self):
        if self.is_valid():
            for test, new_name in self.affected_tests.items():
                test.rename(new_name)
