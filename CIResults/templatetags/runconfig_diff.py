from django import template


# Define some filters for django's
register = template.Library()


@register.filter
def show_suppressed(value):
    return value.name if value.vetted else "{"+value.name+"}"


@register.filter
def show_test(test):
    new = " (NEW)" if test.first_runconfig is None else ""
    return "{}{}".format(show_suppressed(test), new)
