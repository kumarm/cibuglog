from django.test import TestCase
from django.utils import timezone
from datetime import timedelta
from unittest.mock import patch

from CIResults.templatetags.runconfig_diff import show_test
from CIResults.templatetags.helpers import howlongago, csl

from CIResults.models import Test, RunConfig, Machine


class ShowTestTests(TestCase):
    def test_new_test(self):
        test = Test(name="test", first_runconfig=None, vetted_on=None)
        self.assertEqual(show_test(test), "{test} (NEW)")

    def test_suppressed_test(self):
        test = Test(name="test", first_runconfig=RunConfig(), vetted_on=None)
        self.assertEqual(show_test(test), "{test}")

    def test_active_test(self):
        test = Test(name="test", first_runconfig=RunConfig(), vetted_on=timezone.now())
        self.assertEqual(show_test(test), "test")


class HowLongAgoTests(TestCase):
    def test_None(self):
        self.assertEqual(howlongago(None), "Never")

    @patch('django.utils.timezone.now', return_value=timezone.now())
    def test_valid_datetime(self, now_mock):
        date = now_mock.return_value - timedelta(days=1, hours=3, seconds=2)
        self.assertEqual(howlongago(date), "1 day, 3 hours ago")


class CslTests(TestCase):
    def test_empty_list(self):
        self.assertEqual(csl([], "machine:CIResults/basic/machine.html"), "")

    def test_list_of_machines(self):
        machines = []
        for m in range(3):
            machines.append(Machine.objects.create(name='machine{}'.format(m), public=True))

        result = csl(machines, "machine:CIResults/basic/machine.html")

        for m in range(3):
            self.assertIn('machine{}'.format(m), result)
