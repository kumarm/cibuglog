from django.test import TestCase
from CIResults.context_processors import global_context

import os


class global_contextTests(TestCase):
    def test_cibuglog(self):
        cibuglog = global_context(None)['cibuglog']

        version = 'Hello world'
        os.environ.pop('CIBUGLOG_VERSION', None)
        self.assertEqual(cibuglog.version, None)
        os.environ['CIBUGLOG_VERSION'] = version
        self.assertEqual(cibuglog.version, version)

        self.assertIn('freedesktop.org', cibuglog.project_url)
        self.assertIn(version, cibuglog.version_url)
