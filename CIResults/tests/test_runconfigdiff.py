from unittest.mock import patch, Mock, MagicMock, PropertyMock
from django.test import TestCase
from django.utils import timezone
from datetime import timedelta

from CIResults.models import Component, Build, Test, Machine, RunConfig, TestSuite
from CIResults.models import TextStatus, TestResult
from CIResults.runconfigdiff import RunConfigResultsForTest, RunConfigResultsForTestDiff
from CIResults.runconfigdiff import RunConfigResultsForNotRunTest, ExecutionTime


class ExecutionTimeTests(TestCase):
    def test_empty(self):
        empty = ExecutionTime()

        self.assertEqual(empty.minimum, None)
        self.assertEqual(empty.maximum, None)
        self.assertEqual(empty.count, 0)

    def test_add__normal_case(self):
        val = ExecutionTime(23) + ExecutionTime(24)

        self.assertEqual(val.minimum, 23)
        self.assertEqual(val.maximum, 24)
        self.assertEqual(str(val), "[23, 24] s")

    def test_add__with_empty(self):
        val = ExecutionTime() + ExecutionTime(23) + ExecutionTime()
        self.assertEqual(val, ExecutionTime(23))
        self.assertEqual(str(val), "[23] s")

    def test_equal(self):
        # Check that arriving at the same result twice leads to values being equal
        val1 = ExecutionTime(23) + ExecutionTime(24)
        val2 = ExecutionTime(23) + ExecutionTime(24)
        self.assertEqual(val1, val2)

        # Check that re-adding a value indeed does not result in the results being equal
        val2 += ExecutionTime(23)
        self.assertNotEqual(val1, val2)

    def test_str__with_timedelta(self):
        self.assertEqual(str(ExecutionTime(timedelta(seconds=0.987654321))), "[0.99] s")
        self.assertEqual(str(ExecutionTime(timedelta(seconds=12345.987654321))), "[12345.99] s")


class RunConfigResultsForTestTests(TestCase):
    def __create_testsuite(self, name, statuses, acceptable_statuses=['pass'], public=True):
        testsuite = TestSuite.objects.create(name=name, description="nothing", public=public)

        db_statuses = dict()
        for status_name in statuses:
            status = TextStatus.objects.create(testsuite=testsuite, name=status_name,
                                               vetted_on=timezone.now())
            db_statuses[status_name] = status

        for status_name in acceptable_statuses:
            testsuite.acceptable_statuses.add(db_statuses[status_name])

        return testsuite, db_statuses

    def test_RunConfigResultsForTests_check_no_results(self):
        self.assertRaisesMessage(ValueError, "No results provided", RunConfigResultsForTest, [])

    def test_RunConfigResultsForTests_check_single_result(self):
        self.testsuite1, self.testsuite1_statuses = self.__create_testsuite("testsuite1",
                                                                            ['pass', 'fail'],
                                                                            ['pass'],
                                                                            public=True)
        self.testsuite1_statuses["fail"].suppress()

        test = Test.objects.create(name="Test", testsuite=self.testsuite1, public=True)
        result_pass = TestResult(test=test, status=self.testsuite1_statuses['pass'])
        result_fail = TestResult(test=test, status=self.testsuite1_statuses['fail'])

        r = RunConfigResultsForTest([result_pass])
        self.assertTrue(r.was_run)
        self.assertEqual(str(r), "pass")
        self.assertEqual(r, RunConfigResultsForTest([result_pass]))
        self.assertNotEqual(r, RunConfigResultsForTest([result_fail]))

        # Now try with a suppressed status
        result_fail = TestResult(test=test, status=self.testsuite1_statuses['fail'])
        r = RunConfigResultsForTest([result_fail])
        self.assertEqual(str(r), "{fail}")

    def test_RunConfigResultsForTests_check_results_from_different_testsuites(self):
        self.testsuite1, self.testsuite1_statuses = self.__create_testsuite("testsuite1",
                                                                            ['pass'], [],
                                                                            public=True)
        self.testsuite2, self.testsuite2_statuses = self.__create_testsuite("testsuite2",
                                                                            ['pass'], [],
                                                                            public=True)

        test1 = Test.objects.create(name="Test", testsuite=self.testsuite1, public=True)
        test2 = Test.objects.create(name="Test", testsuite=self.testsuite2, public=True)

        result_pass_ts1 = TestResult(test=test1, status=self.testsuite1_statuses['pass'])
        result_pass_ts2 = TestResult(test=test2, status=self.testsuite2_statuses['pass'])

        self.assertRaisesMessage(ValueError, "Results from multiple tests",
                                 RunConfigResultsForTest, [result_pass_ts1, result_pass_ts2])

    def test_RunConfigResultsForTests_check_two_results_same_status(self):
        self.testsuite1, self.testsuite1_statuses = self.__create_testsuite("testsuite1",
                                                                            ['pass', 'fail'], [],
                                                                            public=True)
        self.testsuite1_statuses["fail"].suppress()

        test = Test.objects.create(name="Test", testsuite=self.testsuite1, public=True)
        result_pass1 = TestResult(test=test, status=self.testsuite1_statuses['pass'])
        result_pass2 = TestResult(test=test, status=self.testsuite1_statuses['pass'])
        result_fail = TestResult(test=test, status=self.testsuite1_statuses['fail'])

        r = RunConfigResultsForTest([result_pass1, result_pass2])
        self.assertEqual(str(r), "( 2 pass )")
        self.assertEqual(r, RunConfigResultsForTest([result_pass1, result_pass2]))
        self.assertNotEqual(r, RunConfigResultsForTest([result_pass1, result_fail]))

        # Now try with a suppressed status
        result_fail1 = TestResult(test=test, status=self.testsuite1_statuses['fail'])
        result_fail2 = TestResult(test=test, status=self.testsuite1_statuses['fail'])
        r = RunConfigResultsForTest([result_fail1, result_fail2])
        self.assertEqual(str(r), "( 2 {fail} )")

    def test_RunConfigResultsForTests_check_same_statuses_but_different_bugs(self):
        self.testsuite1, self.testsuite1_statuses = self.__create_testsuite("testsuite1",
                                                                            ['pass', 'fail'], [],
                                                                            public=True)
        self.testsuite1_statuses["fail"].suppress()

        test = Test.objects.create(name="Test", testsuite=self.testsuite1, public=True)
        result_pass = TestResult(test=test, status=self.testsuite1_statuses['pass'])
        result_fail = TestResult(test=test, status=self.testsuite1_statuses['fail'])

        r1 = RunConfigResultsForTest([result_fail, result_pass])
        r1.bugs_covering = set(['bug1', 'bug2'])

        r2 = RunConfigResultsForTest([result_pass, result_fail])
        r2.bugs_covering = set(['bug1', 'bug2'])

        self.assertEqual(r1, r2)

        r2.bugs_covering = set(['bug1', 'bug3'])
        self.assertNotEqual(r1, r2)

    def test_RunConfigResultsForTests_check_two_results_different_status(self):
        self.testsuite1, self.testsuite1_statuses = self.__create_testsuite("testsuite1",
                                                                            ['pass', 'fail'], [],
                                                                            public=True)
        self.testsuite1_statuses["fail"].suppress()

        test = Test.objects.create(name="Test", testsuite=self.testsuite1, public=True)
        result_pass1 = TestResult(test=test, status=self.testsuite1_statuses['pass'])
        result_pass2 = TestResult(test=test, status=self.testsuite1_statuses['pass'])
        result_fail = TestResult(test=test, status=self.testsuite1_statuses['fail'])

        r = RunConfigResultsForTest([result_pass1, result_fail, result_pass2])
        self.assertEqual(str(r), "( 1 {fail}, 2 pass )")
        self.assertEqual(r, RunConfigResultsForTest([result_pass1, result_pass2, result_fail]))
        self.assertEqual(r, RunConfigResultsForTest([result_pass1, result_fail]))

    def test_RunConfigResultsForTests_check_not_runs_ignored(self):
        self.testsuite1, self.testsuite1_statuses = self.__create_testsuite("testsuite1",
                                                                            ['pass', 'fail', 'notrun'],
                                                                            [], public=True)
        self.testsuite1.notrun_status = self.testsuite1_statuses['notrun']
        self.testsuite1.save()

        test = Test.objects.create(name="Test", testsuite=self.testsuite1, public=True)
        result_pass = TestResult(test=test, status=self.testsuite1_statuses['pass'])
        result_notrun = TestResult(test=test, status=self.testsuite1_statuses['notrun'])
        result_fail = TestResult(test=test, status=self.testsuite1_statuses['fail'])

        r = RunConfigResultsForTest([result_pass, result_notrun, result_fail])
        self.assertEqual(str(r), "( 1 fail, 1 pass )")
        self.assertEqual(r, RunConfigResultsForTest([result_pass, result_fail]))

    def test_RunConfigResultsForTests_check_only_not_runs(self):
        self.testsuite1, self.testsuite1_statuses = self.__create_testsuite("testsuite1",
                                                                            ['pass', 'fail', 'notrun'],
                                                                            [], public=True)
        self.testsuite1.notrun_status = self.testsuite1_statuses['notrun']
        self.testsuite1.save()

        test = Test.objects.create(name="Test", testsuite=self.testsuite1, public=True)
        result_notrun1 = TestResult(test=test, status=self.testsuite1_statuses['notrun'])
        result_notrun2 = TestResult(test=test, status=self.testsuite1_statuses['notrun'])
        result_notrun3 = TestResult(test=test, status=self.testsuite1_statuses['notrun'])

        self.assertRaisesMessage(ValueError, "No results provided",
                                 RunConfigResultsForTest, [result_notrun1, result_notrun2, result_notrun3])

    def test_RunConfigResultsForTests_is_suppressed(self):
        self.testsuite1, self.testsuite1_statuses = self.__create_testsuite("testsuite1",
                                                                            ['pass', 'fail'], [],
                                                                            public=True)
        self.testsuite1.acceptable_statuses.add(self.testsuite1_statuses['pass'])

        test = Test.objects.create(name="Test", testsuite=self.testsuite1, public=True)
        result_pass1 = TestResult(test=test, status=self.testsuite1_statuses['pass'])
        result_pass2 = TestResult(test=test, status=self.testsuite1_statuses['pass'])
        result_fail = TestResult(test=test, status=self.testsuite1_statuses['fail'])

        # Try without suppressing fails
        r = RunConfigResultsForTest([result_pass1, result_fail, result_pass2])
        self.assertFalse(r.is_suppressed)

        # Try again, with fail suppressed
        self.testsuite1_statuses["fail"].suppress()
        r = RunConfigResultsForTest([result_pass1, result_fail, result_pass2])
        self.assertTrue(r.is_suppressed)


class RunConfigResultsForTestDiffTests(TestCase):
    def setUp(self):
        first_runconfig = RunConfig.objects.create(name="Runconfig", temporary=False)

        self.testsuite = TestSuite.objects.create(name="testsuite", public=True,
                                                  vetted_on=timezone.now())
        self.test = Test.objects.create(name="test1", testsuite=self.testsuite, public=True,
                                        vetted_on=timezone.now(), first_runconfig=first_runconfig)
        self.machine = Machine.objects.create(name="machine1", public=True,
                                              vetted_on=timezone.now())

    def test_is_fix(self):
        result_from = MagicMock(spec="RunConfigResultsForTest", __str__=MagicMock(return_value="FAIL"),
                                is_failure=True, bugs_covering=[], all_failures_covered=False, is_suppressed=False)
        result_to = MagicMock(spec="RunConfigResultsForTest", __str__=MagicMock(return_value="PASS"),
                              is_failure=False, bugs_covering=[], all_failures_covered=True, is_suppressed=False)

        r = RunConfigResultsForTestDiff(test=self.test, testsuite=self.testsuite,
                                        machine=self.machine, result_from=result_from,
                                        result_to=result_to)

        self.assertTrue(r.is_fix and r.is_known_change)
        self.assertFalse(r.is_regression or r.is_warning or r.is_unknown_change or r.is_regression)
        self.assertEqual(str(r), "machine1:           FAIL -> PASS")

    def test_is_regression(self):
        result_from = MagicMock(spec="RunConfigResultsForTest", __str__=MagicMock(return_value="PASS"),
                                is_failure=False, bugs_covering=[], all_failures_covered=True, is_suppressed=False)
        result_to = MagicMock(spec="RunConfigResultsForTest", __str__=MagicMock(return_value="FAIL"),
                              is_failure=True, bugs_covering=[], all_failures_covered=False, is_suppressed=False)

        r = RunConfigResultsForTestDiff(test=self.test, testsuite=self.testsuite,
                                        machine=self.machine, result_from=result_from,
                                        result_to=result_to)

        self.assertTrue(r.is_regression and r.is_unknown_change)
        self.assertFalse(r.is_fix or r.is_warning or r.is_suppressed or r.is_known_change)
        self.assertEqual(str(r), "machine1:           PASS -> FAIL")

    def test_is_warning(self):
        result_from = MagicMock(spec="RunConfigResultsForTest", __str__=MagicMock(return_value="FAIL"),
                                is_failure=True, bugs_covering=[], all_failures_covered=False, is_suppressed=False)
        result_to = MagicMock(spec="RunConfigResultsForTest", __str__=MagicMock(return_value="FAIL"),
                              is_failure=True, bugs_covering=[], all_failures_covered=False, is_suppressed=False)

        r = RunConfigResultsForTestDiff(test=self.test, testsuite=self.testsuite,
                                        machine=self.machine, result_from=result_from,
                                        result_to=result_to)

        self.assertTrue(r.is_warning and r.is_unknown_change)
        self.assertFalse(r.is_fix or r.is_regression or r.is_regression or r.is_known_change)
        self.assertEqual(str(r), "machine1:           FAIL -> FAIL")

    def test_is_suppressed(self):
        result_from = MagicMock(spec="RunConfigResultsForTest", __str__=MagicMock(return_value="PASS"),
                                is_failure=True, bugs_covering=[], all_failures_covered=False, is_suppressed=False)
        result_to = MagicMock(spec="RunConfigResultsForTest", __str__=MagicMock(return_value="{FAIL}"),
                              is_failure=True, bugs_covering=[], all_failures_covered=False, is_suppressed=True)

        r = RunConfigResultsForTestDiff(test=self.test, testsuite=self.testsuite,
                                        machine=self.machine, result_from=result_from,
                                        result_to=result_to)

        self.assertTrue(r.is_suppressed and r.is_unknown_change)
        self.assertFalse(r.is_fix or r.is_regression or r.is_warning or r.is_known_change)
        self.assertEqual(str(r), "machine1:           PASS -> {FAIL}")

    def test_is_known_change(self):
        result_from = MagicMock(spec="RunConfigResultsForTest", __str__=MagicMock(return_value="FAIL"),
                                is_failure=True, all_failures_covered=True, is_suppressed=False,
                                bugs_covering=[MagicMock(spec="Bug", short_name="fdo#1234"),
                                               MagicMock(spec="Bug", short_name="fdo#1235")])
        result_to = MagicMock(spec="RunConfigResultsForTest", __str__=MagicMock(return_value="FAIL"),
                              is_failure=True, all_failures_covered=True, is_suppressed=False,
                              bugs_covering=[MagicMock(spec="Bug", short_name="fdo#1236"),
                                             MagicMock(spec="Bug", short_name="fdo#1237")])

        r = RunConfigResultsForTestDiff(test=self.test, testsuite=self.testsuite,
                                        machine=self.machine, result_from=result_from,
                                        result_to=result_to)

        self.assertTrue(r.is_warning and r.is_known_change)
        self.assertFalse(r.is_regression or r.is_fix or r.is_unknown_change)
        self.assertEqual(str(r), "machine1:           FAIL ([fdo#1234] / [fdo#1235]) -> FAIL ([fdo#1236] / [fdo#1237])")

    def test_is_new_test(self):
        result_from = RunConfigResultsForNotRunTest()
        result_to = MagicMock(spec="RunConfigResultsForTest", __str__=MagicMock(return_value="FAIL"),
                              is_failure=True, bugs_covering=[], all_failures_covered=False, is_suppressed=False)

        new_test = Test.objects.create(name="new_test", testsuite=self.testsuite, public=True,
                                       vetted_on=timezone.now(), first_runconfig=None)
        r = RunConfigResultsForTestDiff(test=new_test, testsuite=self.testsuite,
                                        machine=self.machine, result_from=result_from,
                                        result_to=result_to)

        self.assertTrue(r.is_new_test and r.is_regression and r.is_unknown_change)
        self.assertFalse(r.is_fix or r.is_warning or r.is_known_change)
        self.assertEqual(str(r), "machine1:           NOTRUN -> FAIL")


class RunConfigDiffTests(TestCase):
    def setUp(self):
        self.runcfg_from = RunConfig.objects.create(name="runcfg_from", temporary=False)
        self.runcfg_to = RunConfig.objects.create(name="runcfg_to", temporary=False)

        self.component1 = Component.objects.create(name="component1", description="", public=True)
        self.component2 = Component.objects.create(name="component2", description="", public=True)
        self.component3 = Component.objects.create(name="component3", description="", public=True)

        self.build_c1_1 = Build.objects.create(name="build_c1_1", component=self.component1, version="1")
        self.build_c1_2 = Build.objects.create(name="build_c1_2", component=self.component1, version="2")
        self.build_c2_1 = Build.objects.create(name="build_c2_1", component=self.component2, version="1")
        self.build_c2_2 = Build.objects.create(name="build_c2_2", component=self.component2, version="2")
        self.build_c3_1 = Build.objects.create(name="build_c3_1", component=self.component3, version="1")

        self.machines = []
        for i in range(10):
            self.machines.append(Mock(spec=Machine, name='machine_{}'.format(i)))

    def test_builds__simple(self):
        self.runcfg_from.builds.add(self.build_c1_1, self.build_c2_1, self.build_c3_1)
        self.runcfg_to.builds.add(self.build_c1_2, self.build_c2_2, self.build_c3_1)

        diff = self.runcfg_from.compare(self.runcfg_to)
        diff_builds = diff.builds

        self.assertEqual(len(diff_builds), 2)

        self.assertEqual(diff_builds[self.component1].from_build, self.build_c1_1)
        self.assertEqual(diff_builds[self.component1].to_build, self.build_c1_2)

        self.assertEqual(diff_builds[self.component2].from_build, self.build_c2_1)
        self.assertEqual(diff_builds[self.component2].to_build, self.build_c2_2)

        self.assertIn("component1: build_c1_1 -> build_c1_2", diff.text)
        self.assertIn("component2: build_c2_1 -> build_c2_2", diff.text)
        self.assertNotIn("component3", diff.text)

    def test_builds__asymmetric(self):
        self.runcfg_from.builds.add(self.build_c1_1)
        self.runcfg_to.builds.add(self.build_c2_2)

        diff = self.runcfg_from.compare(self.runcfg_to)
        diff_builds = diff.builds

        self.assertEqual(len(diff_builds), 2)

        self.assertEqual(diff_builds[self.component1].from_build, self.build_c1_1)
        self.assertEqual(diff_builds[self.component1].to_build, None)

        self.assertEqual(diff_builds[self.component2].from_build, None)
        self.assertEqual(diff_builds[self.component2].to_build, self.build_c2_2)

        self.assertIn("component1: build_c1_1 -> None", diff.text)
        self.assertIn("component2: None -> build_c2_2", diff.text)
        self.assertNotIn("component3", diff.text)

    def test_testsuites__no_results(self):
        diff = self.runcfg_from.compare(self.runcfg_to)

        self.assertIn("No changes found", diff.text)

    @patch('CIResults.runconfigdiff.RunConfigDiff.runcfg_from_results', new_callable=PropertyMock)
    @patch('CIResults.runconfigdiff.RunConfigDiff.runcfg_to_results', new_callable=PropertyMock)
    def test_testsuites__all_combinaisons(self, mock_runcfg_to_results, mock_runcfg_from_results):
        mock_runcfg_from_results.return_value = {"testsuite3": {}, "testsuite1": {}}
        mock_runcfg_to_results.return_value = {"testsuite2": {}, "testsuite1": {}}

        diff = self.runcfg_from.compare(self.runcfg_to)

        self.assertEqual(diff.testsuites.runcfg_from, set(mock_runcfg_from_results.return_value.keys()))
        self.assertEqual(diff.testsuites.runcfg_to, set(mock_runcfg_to_results.return_value.keys()))
        self.assertEqual(diff.testsuites.new, set(["testsuite2"]))
        self.assertEqual(diff.testsuites.removed, set(["testsuite3"]))
        self.assertEqual(diff.testsuites.all, ["testsuite1", "testsuite2", "testsuite3"])

    def test_has_sufficient_machines__no_machines(self):
        diff = self.runcfg_from.compare(self.runcfg_to)
        self.assertTrue(diff.has_sufficient_machines)
        self.assertEqual(diff.status, "SUCCESS")

    def test_has_sufficient_machines__same_machines(self):
        m = self.machines
        with patch('CIResults.runconfigdiff.RunConfigDiff._get_machine_list',
                   side_effect=[set([m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7], m[8], m[9]]),
                                set([m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7], m[8], m[9]])]):
            diff = self.runcfg_from.compare(self.runcfg_to)
            self.assertEqual(len(diff.machines.runcfg_from), 10)
            self.assertEqual(len(diff.machines.removed), 0)
            self.assertTrue(diff.has_sufficient_machines)
            self.assertEqual(diff.status, "SUCCESS")

    def test_has_sufficient_machines__at_threshold(self):
        m = self.machines

        with patch('CIResults.runconfigdiff.RunConfigDiff._get_machine_list',
                   side_effect=[set([m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7], m[8], m[9]]),
                                set([m[0], m[3], m[4], m[5], m[6], m[9]])]):
            diff = self.runcfg_from.compare(self.runcfg_to, 0.4)
            self.assertEqual(len(diff.machines.runcfg_from), 10)
            self.assertEqual(len(diff.machines.removed), 4)
            self.assertTrue(diff.has_sufficient_machines)
            self.assertEqual(diff.status, "SUCCESS")
            self.assertNotIn("prevented too many machines from booting", diff.text)

    def test_has_sufficient_machines__under_threshold(self):
        m = self.machines
        with patch('CIResults.runconfigdiff.RunConfigDiff._get_machine_list',
                   side_effect=[set([m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7], m[8], m[9]]),
                                set([m[0], m[3], m[4], m[5], m[8]])]):
            diff = self.runcfg_from.compare(self.runcfg_to, 0.4)
            self.assertEqual(len(diff.machines.runcfg_from), 10)
            self.assertEqual(len(diff.machines.removed), 5)
            self.assertFalse(diff.has_sufficient_machines)
            self.assertEqual(diff.status, "FAILURE")
            self.assertIn("prevented too many machines from booting", diff.text)

    def test_has_suppressed_results(self):
        diff = self.runcfg_from.compare(self.runcfg_to, 0.4)
        diff.results = [MagicMock(), MagicMock(), MagicMock()]

        # Mock the return values of the is_suppressed property
        p_true = PropertyMock(return_value=True)
        p_false = PropertyMock(return_value=False)
        type(diff.results[0]).is_suppressed = p_false
        type(diff.results[1]).is_suppressed = p_true
        type(diff.results[2]).is_suppressed = p_false

        self.assertTrue(diff.has_suppressed_results)

        p_true.assert_called_once_with()
        p_false.assert_called_once_with()

    # TODO: Test machines, bugs, status (with suppressed results)
