#!/usr/bin/env python3

from multiprocessing import Value
from multiprocessing.dummy import Pool
from datetime import timedelta
from django.utils import timezone

import django
import argparse
import traceback

django.setup()

from CIResults.models import BugTracker, Bug, BugComment  # noqa


def poll_bug(bug):
    try:
        bug.poll(force_polling_comments=args.force_polling_comments)
        bug.save()
    except Exception:
        traceback.print_exc()

    with polled.get_lock():
        polled.value += 1
        print("{}/{}: polled {}".format(polled.value, len(bugs), bug.short_name))


# Parse the options
parser = argparse.ArgumentParser()
parser.add_argument("-f", "--force-polling-comments", action="store_true",
                    help="Re-download all the comments associated to bugs")
parser.add_argument("-d", '--delay', default=8640000, type=int,
                    help="Minimum delay (seconds) that needs to have passed before re-polling the bug. Default: 86400")
parser.add_argument("-r", '--remove', default=0, type=int,
                    help="Number of days a bug needs to have been closed before being removed from the database "
                         "(bugs referenced in an issue are not deleted). Use 0 to never remove any bug. Default: 180")
parser.add_argument("-j", '--jobs', default=10, type=int,
                    help="How many parallel jobs should be created to poll the bugs. Default: 10")
args = parser.parse_args()

# Get the full list of bugs that are followed
bugs = set()
for bt in BugTracker.objects.all():
    try:
        followed_bugs = bt.followed_bugs()
        print("{}: Found {} bugs to follow".format(bt, len(followed_bugs)))
    except Exception:
        traceback.print_exc()

    # add to the list of bugs to poll
    bugs.update(followed_bugs)
print()

# Remove all the bugs that have been polled less than the specified delay
bugs = [bug for bug in bugs if bug.polled is None or timezone.now() - bug.polled > timedelta(seconds=args.delay)]

# Start polling in parallel (10 threads) to speed up the polling
print("Polling {} outdated bugs needing to be polled (older than {} seconds)".format(len(bugs), args.delay))
if len(bugs) > 0:
    polled = Value('i', 0)
    with Pool(processes=args.jobs) as pool:
        pool.map(poll_bug, bugs)
else:
    print("Found no bugs to poll. Did you set BugTracker.components_followed_since?")

# Update when the tracker was last polled
for bt in BugTracker.objects.all():
    bt.polled = timezone.now()
    bt.save()

# Look for all the bugs that are closed and not referenced by any issue
if args.remove > 0:
    deleted_count = 0
    remove_threshold = timezone.now() - timedelta(days=args.remove)
    for bug in Bug.objects.filter(issue=None, closed__lt=remove_threshold).prefetch_related('tracker'):
        if not bug.is_open:
            try:
                bug.delete()
                deleted_count += 1
            except Exception as e:
                print(e)
    print("\nFound {} bugs to be deleted".format(deleted_count))
